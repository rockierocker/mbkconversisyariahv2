// 
// Decompiled by Procyon v0.5.36
// 

package view;

import java.awt.EventQueue;
import java.awt.event.WindowListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowAdapter;
import javax.swing.JFrame;
import javax.swing.UnsupportedLookAndFeelException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.UIManager;
import javax.swing.JOptionPane;
import javax.swing.LayoutStyle;
import java.awt.Component;
import java.awt.LayoutManager;
import java.awt.Container;
import javax.swing.GroupLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Color;
import java.awt.Font;
import java.awt.Dimension;
import java.awt.Frame;
import javax.swing.JTextField;
import javax.swing.JScrollPane;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JButton;
import model.sheet.S214021350_TUP;
import javax.swing.JDialog;

public class DialogValidateS214021350 extends JDialog
{
    private S214021350_TUP s214021350_tup;
    private Boolean isValidateMonetary;
    private Boolean castAllRowToMonetary;
    private JButton btnConfirm;
    private JButton btnExitApplication;
    private JLabel jLabel1;
    private JLabel jLabel10;
    private JLabel jLabel11;
    private JLabel jLabel12;
    private JLabel jLabel13;
    private JLabel jLabel14;
    private JLabel jLabel15;
    private JLabel jLabel16;
    private JLabel jLabel17;
    private JLabel jLabel18;
    private JLabel jLabel19;
    private JLabel jLabel2;
    private JLabel jLabel20;
    private JLabel jLabel21;
    private JLabel jLabel22;
    private JLabel jLabel23;
    private JLabel jLabel24;
    private JLabel jLabel25;
    private JLabel jLabel26;
    private JLabel jLabel27;
    private JLabel jLabel28;
    private JLabel jLabel29;
    private JLabel jLabel3;
    private JLabel jLabel30;
    private JLabel jLabel31;
    private JLabel jLabel32;
    private JLabel jLabel33;
    private JLabel jLabel34;
    private JLabel jLabel35;
    private JLabel jLabel36;
    private JLabel jLabel38;
    private JLabel jLabel39;
    private JLabel jLabel4;
    private JLabel jLabel40;
    private JLabel jLabel41;
    private JLabel jLabel42;
    private JLabel jLabel43;
    private JLabel jLabel44;
    private JLabel jLabel45;
    private JLabel jLabel46;
    private JLabel jLabel47;
    private JLabel jLabel48;
    private JLabel jLabel49;
    private JLabel jLabel5;
    private JLabel jLabel50;
    private JLabel jLabel51;
    private JLabel jLabel52;
    private JLabel jLabel53;
    private JLabel jLabel54;
    private JLabel jLabel55;
    private JLabel jLabel56;
    private JLabel jLabel57;
    private JLabel jLabel58;
    private JLabel jLabel59;
    private JLabel jLabel60;
    private JLabel jLabel61;
    private JLabel jLabel62;
    private JLabel jLabel63;
    private JLabel jLabel64;
    private JLabel jLabel65;
    private JLabel jLabel66;
    private JLabel jLabel7;
    private JLabel jLabel8;
    private JLabel jLabel9;
    private JPanel jPanel1;
    private JScrollPane jScrollPane1;
    private JLabel lblBentukPasanganUsahaDebitur;
    private JLabel lblBungaBagiHasilYangDiTangguhkanDalamEquivalenRupiah;
    private JLabel lblBungaBagiHasilYangDiTangguhkanDalamMataUangAsal;
    private JLabel lblGolonganPasanganUsahaDebitur;
    private JLabel lblJenisAgunan;
    private JLabel lblJenisBagiHasil;
    private JLabel lblJenisValuta;
    private JLabel lblKategoriUsahaKeuanganBerkelanjutan;
    private JLabel lblKategoriUsahaPasanganDebitur;
    private JLabel lblKualitas;
    private JLabel lblLokasiKabupatenKota;
    private JLabel lblMetodePembentukanCadanganKerugian;
    private JLabel lblNamaPasanganUsahaDebitur;
    private JLabel lblNilaiAgunan;
    private JLabel lblNilaiAsetBaikCadanganKerugianPenurunanNilai;
    private JLabel lblNilaiAsetKurangBaikCadanganKerugianPenurunanNilai;
    private JLabel lblNilaiAsetTidakBaikCadanganKerugianPenurunanNilai;
    private JLabel lblNilaiAwalPembiayaan;
    private JLabel lblNoKontrak;
    private JLabel lblNomorAgunan;
    private JLabel lblPiutangPembiayaanPokokDalamEquivalenRupiah;
    private JLabel lblPiutangPembiayaanPokokDalamMataUangAsal;
    private JLabel lblProporsiPenjaminanPembiayaan;
    private JLabel lblSektorEknomi;
    private JLabel lblStatusKeterkaitan;
    private JLabel lblTagihanPiutangPembiayaanBrutoDalamEquivalenRupiah;
    private JLabel lblTagihanPiutangPembiayaanBrutoDalamMataUangAsal;
    private JLabel lblTanggalJatuhTempo;
    private JLabel lblTanggalMulaiPembiayaan;
    private JLabel lblTingkatBagiHasil;
    private JTextField txtBentukPasanganUsahaDebitur;
    private JTextField txtBungaBagiHasilYangDiTangguhkanDalamEquivalenRupiah;
    private JTextField txtBungaBagiHasilYangDiTangguhkanDalamMataUangAsal;
    private JTextField txtGolonganPasanganUsahaDebitur;
    private JTextField txtId;
    private JTextField txtJenisAgunan;
    private JTextField txtJenisBagiHasil;
    private JTextField txtJenisValuta;
    private JTextField txtKategoriUsahaKeuanganBerkelanjutan;
    private JTextField txtKategoriUsahaPasanganDebitur;
    private JTextField txtKualitas;
    private JTextField txtLokasiKabupatenKota;
    private JTextField txtMetodePembentukanCadanganKerugian;
    private JTextField txtNamaPasanganUsahaDebitur;
    private JTextField txtNilaiAgunan;
    private JTextField txtNilaiAsetBaikCadanganKerugianPenurunanNilai;
    private JTextField txtNilaiAsetKurangBaikCadanganKerugianPenurunanNilai;
    private JTextField txtNilaiAsetTidakBaikCadanganKerugianPenurunanNilai;
    private JTextField txtNilaiAwalPembiayaan;
    private JTextField txtNo;
    private JTextField txtNoKontrak;
    private JTextField txtNomorAgunan;
    private JTextField txtPiutangPembiayaanPokokDalamEquivalenRupiah;
    private JTextField txtPiutangPembiayaanPokokDalamMataUangAsal;
    private JTextField txtProporsiPenjaminanPembiayaan;
    private JTextField txtSektorEknomi;
    private JTextField txtStatusKeterkaitan;
    private JTextField txtTagihanPiutangPembiayaanBrutoDalamEquivalenRupiah;
    private JTextField txtTagihanPiutangPembiayaanBrutoDalamMataUangAsal;
    private JTextField txtTanggalJatuhTempo;
    private JTextField txtTanggalMulaiPembiayaan;
    private JTextField txtTingkatBagiHasil;
    
    public DialogValidateS214021350(final Frame parent, final boolean modal) {
        super(parent, modal);
        this.isValidateMonetary = false;
        this.castAllRowToMonetary = false;
        this.initComponents();
        this.jScrollPane1.setPreferredSize(new Dimension(850, 500));
    }
    
    public S214021350_TUP getS214021350_tup() {
        this.s214021350_tup.setNoKontrak(this.txtNoKontrak.getText());
        this.s214021350_tup.setTanggalMulaiPembiayaan(this.txtTanggalMulaiPembiayaan.getText());
        this.s214021350_tup.setTanggalJatuhTempo(this.txtTanggalJatuhTempo.getText());
        this.s214021350_tup.setJenisBagiHasil(this.txtJenisBagiHasil.getText());
        this.s214021350_tup.setTingkatBagiHasil(this.txtTingkatBagiHasil.getText());
        this.s214021350_tup.setNilaiAwalPembiayaan(this.txtNilaiAwalPembiayaan.getText());
        this.s214021350_tup.setKualitas(this.txtKualitas.getText());
        this.s214021350_tup.setJenisValuta(this.txtJenisValuta.getText());
        this.s214021350_tup.setTagihanPiutangPembiayaanBrutoDalamMataUangAsal(this.txtTagihanPiutangPembiayaanBrutoDalamMataUangAsal.getText());
        this.s214021350_tup.setTagihanPiutangPembiayaanBrutoDalamEquivalenRupiah(this.txtTagihanPiutangPembiayaanBrutoDalamEquivalenRupiah.getText());
        this.s214021350_tup.setBungaBagiHasilYangDiTangguhkanDalamMataUangAsal(this.txtBungaBagiHasilYangDiTangguhkanDalamMataUangAsal.getText());
        this.s214021350_tup.setBungaBagiHasilYangDiTangguhkanDalamEquivalenRupiah(this.txtBungaBagiHasilYangDiTangguhkanDalamEquivalenRupiah.getText());
        this.s214021350_tup.setPiutangPembiayaanPokokDalamMataUangAsal(this.txtPiutangPembiayaanPokokDalamMataUangAsal.getText());
        this.s214021350_tup.setPiutangPembiayaanPokokDalamEquivalenRupiah(this.txtPiutangPembiayaanPokokDalamEquivalenRupiah.getText());
        this.s214021350_tup.setMetodePembentukanCadanganKerugian(this.txtMetodePembentukanCadanganKerugian.getText());
        this.s214021350_tup.setNilaiAsetBaikCadanganKerugianPenurunanNilai(this.txtNilaiAsetBaikCadanganKerugianPenurunanNilai.getText());
        this.s214021350_tup.setNilaiAsetKurangBaikCadanganKerugianPenurunanNilai(this.txtNilaiAsetKurangBaikCadanganKerugianPenurunanNilai.getText());
        this.s214021350_tup.setNilaiAsetTidakBaikCadanganKerugianPenurunanNilai(this.txtNilaiAsetTidakBaikCadanganKerugianPenurunanNilai.getText());
        this.s214021350_tup.setProporsiPenjaminanPembiayaan(this.txtProporsiPenjaminanPembiayaan.getText());
        this.s214021350_tup.setNamaPasanganUsahaDebitur(this.txtNamaPasanganUsahaDebitur.getText());
        this.s214021350_tup.setBentukPasanganUsahaDebitur(this.txtBentukPasanganUsahaDebitur.getText());
        this.s214021350_tup.setKategoriUsahaPasanganDebitur(this.txtKategoriUsahaPasanganDebitur.getText());
        this.s214021350_tup.setKategoriUsahaKeuanganBerkelanjutan(this.txtKategoriUsahaKeuanganBerkelanjutan.getText());
        this.s214021350_tup.setGolonganPasanganUsahaDebitur(this.txtGolonganPasanganUsahaDebitur.getText());
        this.s214021350_tup.setStatusKeterkaitan(this.txtStatusKeterkaitan.getText());
        this.s214021350_tup.setLokasiKabupatenKota(this.txtLokasiKabupatenKota.getText());
        this.s214021350_tup.setSektorEknomi(this.txtSektorEknomi.getText());
        S214021350_TUP.F214021350_COL f214021350_col = null;
        if (this.txtNomorAgunan.getText() != null && !this.txtNomorAgunan.getText().equals("")) {
            f214021350_col = new S214021350_TUP.F214021350_COL();
            f214021350_col.setNomorAgunan(this.txtNomorAgunan.getText());
            f214021350_col.setId(this.s214021350_tup.getId() + "_1");
        }
        if (this.txtJenisAgunan.getText() != null && !this.txtJenisAgunan.getText().equals("")) {
            f214021350_col = ((f214021350_col == null) ? new S214021350_TUP.F214021350_COL() : f214021350_col);
            f214021350_col.setJenisAgunan(this.txtJenisAgunan.getText());
            f214021350_col.setId(this.s214021350_tup.getId() + "_1");
        }
        if (this.txtNilaiAgunan.getText() != null && !this.txtNilaiAgunan.getText().equals("")) {
            f214021350_col = ((f214021350_col == null) ? new S214021350_TUP.F214021350_COL() : f214021350_col);
            f214021350_col.setNilaiAgunan(this.txtNilaiAgunan.getText());
            f214021350_col.setId(this.s214021350_tup.getId() + "_1");
        }
        this.s214021350_tup.setF214021350_col(f214021350_col);
        return this.s214021350_tup;
    }
    
    public void setS214021350_tup(final S214021350_TUP s214021300_tup) {
        this.s214021350_tup = s214021300_tup;
        this.txtId.setText(s214021300_tup.getId());
        this.txtNo.setText(s214021300_tup.getNo().toString());
        this.txtNoKontrak.setText(s214021300_tup.getNoKontrak());
        this.txtTanggalMulaiPembiayaan.setText(s214021300_tup.getTanggalMulaiPembiayaan());
        this.txtTanggalJatuhTempo.setText(s214021300_tup.getTanggalJatuhTempo());
        this.txtJenisBagiHasil.setText(s214021300_tup.getJenisBagiHasil());
        this.txtTingkatBagiHasil.setText(s214021300_tup.getTingkatBagiHasil());
        this.txtNilaiAwalPembiayaan.setText(s214021300_tup.getNilaiAwalPembiayaan());
        this.txtKualitas.setText(s214021300_tup.getKualitas());
        this.txtJenisValuta.setText(s214021300_tup.getJenisValuta());
        this.txtTagihanPiutangPembiayaanBrutoDalamMataUangAsal.setText(s214021300_tup.getTagihanPiutangPembiayaanBrutoDalamMataUangAsal());
        this.txtTagihanPiutangPembiayaanBrutoDalamEquivalenRupiah.setText(s214021300_tup.getTagihanPiutangPembiayaanBrutoDalamEquivalenRupiah());
        this.txtBungaBagiHasilYangDiTangguhkanDalamMataUangAsal.setText(s214021300_tup.getBungaBagiHasilYangDiTangguhkanDalamMataUangAsal());
        this.txtBungaBagiHasilYangDiTangguhkanDalamEquivalenRupiah.setText(s214021300_tup.getBungaBagiHasilYangDiTangguhkanDalamEquivalenRupiah());
        this.txtPiutangPembiayaanPokokDalamMataUangAsal.setText(s214021300_tup.getPiutangPembiayaanPokokDalamMataUangAsal());
        this.txtPiutangPembiayaanPokokDalamEquivalenRupiah.setText(s214021300_tup.getPiutangPembiayaanPokokDalamEquivalenRupiah());
        this.txtMetodePembentukanCadanganKerugian.setText(s214021300_tup.getMetodePembentukanCadanganKerugian());
        this.txtNilaiAsetBaikCadanganKerugianPenurunanNilai.setText(s214021300_tup.getNilaiAsetBaikCadanganKerugianPenurunanNilai());
        this.txtNilaiAsetKurangBaikCadanganKerugianPenurunanNilai.setText(s214021300_tup.getNilaiAsetKurangBaikCadanganKerugianPenurunanNilai());
        this.txtNilaiAsetTidakBaikCadanganKerugianPenurunanNilai.setText(s214021300_tup.getNilaiAsetTidakBaikCadanganKerugianPenurunanNilai());
        this.txtProporsiPenjaminanPembiayaan.setText(s214021300_tup.getProporsiPenjaminanPembiayaan());
        this.txtNamaPasanganUsahaDebitur.setText(s214021300_tup.getNamaPasanganUsahaDebitur());
        this.txtBentukPasanganUsahaDebitur.setText(s214021300_tup.getBentukPasanganUsahaDebitur());
        this.txtKategoriUsahaPasanganDebitur.setText(s214021300_tup.getKategoriUsahaPasanganDebitur());
        this.txtKategoriUsahaKeuanganBerkelanjutan.setText(s214021300_tup.getKategoriUsahaKeuanganBerkelanjutan());
        this.txtGolonganPasanganUsahaDebitur.setText(s214021300_tup.getGolonganPasanganUsahaDebitur());
        this.txtStatusKeterkaitan.setText(s214021300_tup.getStatusKeterkaitan());
        this.txtLokasiKabupatenKota.setText(s214021300_tup.getLokasiKabupatenKota());
        this.txtSektorEknomi.setText(s214021300_tup.getSektorEknomi());
        if (this.s214021350_tup.getF214021350_col() != null) {
            this.txtNomorAgunan.setText(s214021300_tup.getF214021350_col().getNomorAgunan());
            this.txtJenisAgunan.setText(s214021300_tup.getF214021350_col().getJenisAgunan());
            this.txtNilaiAgunan.setText(s214021300_tup.getF214021350_col().getNilaiAgunan());
        }
        this.lblNoKontrak.setText(s214021300_tup.getErrorNoKontrak());
        this.lblTanggalMulaiPembiayaan.setText(s214021300_tup.getErrorTanggalMulaiPembiayaan());
        this.lblTanggalJatuhTempo.setText(s214021300_tup.getErrorTanggalJatuhTempo());
        this.lblJenisBagiHasil.setText(s214021300_tup.getErrorJenisBagiHasil());
        this.lblTingkatBagiHasil.setText(s214021300_tup.getErrorTingkatBagiHasil());
        this.lblNilaiAwalPembiayaan.setText(s214021300_tup.getErrorNilaiAwalPembiayaan());
        this.lblKualitas.setText(s214021300_tup.getErrorKualitas());
        this.lblJenisValuta.setText(s214021300_tup.getErrorJenisValuta());
        this.lblTagihanPiutangPembiayaanBrutoDalamMataUangAsal.setText(s214021300_tup.getErrorTagihanPiutangPembiayaanBrutoDalamMataUangAsal());
        this.lblTagihanPiutangPembiayaanBrutoDalamEquivalenRupiah.setText(s214021300_tup.getErrorTagihanPiutangPembiayaanBrutoDalamEquivalenRupiah());
        this.lblBungaBagiHasilYangDiTangguhkanDalamMataUangAsal.setText(s214021300_tup.getErrorBungaBagiHasilYangDiTangguhkanDalamMataUangAsal());
        this.lblBungaBagiHasilYangDiTangguhkanDalamEquivalenRupiah.setText(s214021300_tup.getErrorBungaBagiHasilYangDiTangguhkanDalamEquivalenRupiah());
        this.lblPiutangPembiayaanPokokDalamMataUangAsal.setText(s214021300_tup.getErrorPiutangPembiayaanPokokDalamMataUangAsal());
        this.lblPiutangPembiayaanPokokDalamEquivalenRupiah.setText(s214021300_tup.getErrorPiutangPembiayaanPokokDalamEquivalenRupiah());
        this.lblMetodePembentukanCadanganKerugian.setText(s214021300_tup.getErrorMetodePembentukanCadanganKerugian());
        this.lblNilaiAsetBaikCadanganKerugianPenurunanNilai.setText(s214021300_tup.getErrorNilaiAsetBaikCadanganKerugianPenurunanNilai());
        this.lblNilaiAsetKurangBaikCadanganKerugianPenurunanNilai.setText(s214021300_tup.getErrorNilaiAsetKurangBaikCadanganKerugianPenurunanNilai());
        this.lblNilaiAsetTidakBaikCadanganKerugianPenurunanNilai.setText(s214021300_tup.getErrorNilaiAsetTidakBaikCadanganKerugianPenurunanNilai());
        this.lblProporsiPenjaminanPembiayaan.setText(s214021300_tup.getErrorProporsiPenjaminanPembiayaan());
        this.lblNamaPasanganUsahaDebitur.setText(s214021300_tup.getErrorNamaPasanganUsahaDebitur());
        this.lblBentukPasanganUsahaDebitur.setText(s214021300_tup.getErrorBentukPasanganUsahaDebitur());
        this.lblKategoriUsahaPasanganDebitur.setText(s214021300_tup.getErrorKategoriUsahaPasanganDebitur());
        this.lblKategoriUsahaKeuanganBerkelanjutan.setText(s214021300_tup.getErrorKategoriUsahaKeuanganBerkelanjutan());
        this.lblGolonganPasanganUsahaDebitur.setText(s214021300_tup.getErrorGolonganPasanganUsahaDebitur());
        this.lblStatusKeterkaitan.setText(s214021300_tup.getErrorStatusKeterkaitan());
        this.lblLokasiKabupatenKota.setText(s214021300_tup.getErrorLokasiKabupatenKota());
        this.lblSektorEknomi.setText(s214021300_tup.getErrorSektorEknomi());
        if (this.s214021350_tup.getF214021350_col() != null) {
            this.lblNomorAgunan.setText(this.s214021350_tup.getF214021350_col().getErrorNomorAgunan());
            this.lblJenisAgunan.setText(this.s214021350_tup.getF214021350_col().getErrorJenisAgunan());
            this.lblNilaiAgunan.setText(this.s214021350_tup.getF214021350_col().getErrorNilaiAgunan());
        }
    }
    
    public void setIsValidateMonetary(final Boolean isValidateMonetary) {
        this.isValidateMonetary = isValidateMonetary;
    }
    
    public Boolean getCastAllRowToMonetary() {
        return this.castAllRowToMonetary;
    }
    
    private void initComponents() {
        this.jScrollPane1 = new JScrollPane();
        this.jPanel1 = new JPanel();
        this.jLabel3 = new JLabel();
        this.jLabel4 = new JLabel();
        this.jLabel5 = new JLabel();
        this.jLabel7 = new JLabel();
        this.jLabel8 = new JLabel();
        this.jLabel9 = new JLabel();
        this.jLabel10 = new JLabel();
        this.jLabel11 = new JLabel();
        this.jLabel12 = new JLabel();
        this.jLabel13 = new JLabel();
        this.jLabel14 = new JLabel();
        this.jLabel15 = new JLabel();
        this.jLabel16 = new JLabel();
        this.jLabel17 = new JLabel();
        this.jLabel18 = new JLabel();
        this.jLabel19 = new JLabel();
        this.jLabel20 = new JLabel();
        this.jLabel21 = new JLabel();
        this.jLabel22 = new JLabel();
        this.jLabel23 = new JLabel();
        this.jLabel24 = new JLabel();
        this.jLabel26 = new JLabel();
        this.jLabel27 = new JLabel();
        this.jLabel28 = new JLabel();
        this.jLabel29 = new JLabel();
        this.jLabel30 = new JLabel();
        this.jLabel31 = new JLabel();
        this.jLabel32 = new JLabel();
        this.jLabel33 = new JLabel();
        this.jLabel34 = new JLabel();
        this.jLabel35 = new JLabel();
        this.jLabel25 = new JLabel();
        this.txtNo = new JTextField();
        this.txtNoKontrak = new JTextField();
        this.txtTanggalMulaiPembiayaan = new JTextField();
        this.txtTanggalJatuhTempo = new JTextField();
        this.txtJenisBagiHasil = new JTextField();
        this.txtTingkatBagiHasil = new JTextField();
        this.txtNilaiAwalPembiayaan = new JTextField();
        this.txtKualitas = new JTextField();
        this.txtJenisValuta = new JTextField();
        this.txtTagihanPiutangPembiayaanBrutoDalamMataUangAsal = new JTextField();
        this.txtTagihanPiutangPembiayaanBrutoDalamEquivalenRupiah = new JTextField();
        this.txtBungaBagiHasilYangDiTangguhkanDalamMataUangAsal = new JTextField();
        this.txtBungaBagiHasilYangDiTangguhkanDalamEquivalenRupiah = new JTextField();
        this.txtPiutangPembiayaanPokokDalamMataUangAsal = new JTextField();
        this.txtPiutangPembiayaanPokokDalamEquivalenRupiah = new JTextField();
        this.txtMetodePembentukanCadanganKerugian = new JTextField();
        this.txtNilaiAsetBaikCadanganKerugianPenurunanNilai = new JTextField();
        this.txtNilaiAsetKurangBaikCadanganKerugianPenurunanNilai = new JTextField();
        this.txtNilaiAsetTidakBaikCadanganKerugianPenurunanNilai = new JTextField();
        this.txtProporsiPenjaminanPembiayaan = new JTextField();
        this.txtNamaPasanganUsahaDebitur = new JTextField();
        this.txtBentukPasanganUsahaDebitur = new JTextField();
        this.txtKategoriUsahaPasanganDebitur = new JTextField();
        this.txtKategoriUsahaKeuanganBerkelanjutan = new JTextField();
        this.txtGolonganPasanganUsahaDebitur = new JTextField();
        this.txtStatusKeterkaitan = new JTextField();
        this.txtLokasiKabupatenKota = new JTextField();
        this.txtSektorEknomi = new JTextField();
        this.txtNomorAgunan = new JTextField();
        this.txtJenisAgunan = new JTextField();
        this.txtNilaiAgunan = new JTextField();
        this.lblNoKontrak = new JLabel();
        this.lblTanggalMulaiPembiayaan = new JLabel();
        this.lblTanggalJatuhTempo = new JLabel();
        this.lblJenisBagiHasil = new JLabel();
        this.lblTingkatBagiHasil = new JLabel();
        this.lblNilaiAwalPembiayaan = new JLabel();
        this.lblKualitas = new JLabel();
        this.lblJenisValuta = new JLabel();
        this.lblTagihanPiutangPembiayaanBrutoDalamMataUangAsal = new JLabel();
        this.lblTagihanPiutangPembiayaanBrutoDalamEquivalenRupiah = new JLabel();
        this.lblBungaBagiHasilYangDiTangguhkanDalamMataUangAsal = new JLabel();
        this.lblBungaBagiHasilYangDiTangguhkanDalamEquivalenRupiah = new JLabel();
        this.lblPiutangPembiayaanPokokDalamMataUangAsal = new JLabel();
        this.lblPiutangPembiayaanPokokDalamEquivalenRupiah = new JLabel();
        this.lblMetodePembentukanCadanganKerugian = new JLabel();
        this.lblNilaiAsetBaikCadanganKerugianPenurunanNilai = new JLabel();
        this.lblNilaiAsetKurangBaikCadanganKerugianPenurunanNilai = new JLabel();
        this.lblNilaiAsetTidakBaikCadanganKerugianPenurunanNilai = new JLabel();
        this.lblProporsiPenjaminanPembiayaan = new JLabel();
        this.lblNamaPasanganUsahaDebitur = new JLabel();
        this.lblBentukPasanganUsahaDebitur = new JLabel();
        this.lblKategoriUsahaPasanganDebitur = new JLabel();
        this.lblKategoriUsahaKeuanganBerkelanjutan = new JLabel();
        this.lblGolonganPasanganUsahaDebitur = new JLabel();
        this.lblStatusKeterkaitan = new JLabel();
        this.lblLokasiKabupatenKota = new JLabel();
        this.lblSektorEknomi = new JLabel();
        this.lblNomorAgunan = new JLabel();
        this.lblJenisAgunan = new JLabel();
        this.lblNilaiAgunan = new JLabel();
        this.jLabel36 = new JLabel();
        this.jLabel38 = new JLabel();
        this.jLabel39 = new JLabel();
        this.jLabel40 = new JLabel();
        this.jLabel41 = new JLabel();
        this.jLabel42 = new JLabel();
        this.jLabel43 = new JLabel();
        this.jLabel44 = new JLabel();
        this.jLabel45 = new JLabel();
        this.jLabel46 = new JLabel();
        this.jLabel47 = new JLabel();
        this.jLabel48 = new JLabel();
        this.jLabel49 = new JLabel();
        this.jLabel50 = new JLabel();
        this.jLabel51 = new JLabel();
        this.jLabel52 = new JLabel();
        this.jLabel53 = new JLabel();
        this.jLabel54 = new JLabel();
        this.jLabel55 = new JLabel();
        this.jLabel56 = new JLabel();
        this.jLabel57 = new JLabel();
        this.jLabel58 = new JLabel();
        this.jLabel59 = new JLabel();
        this.jLabel60 = new JLabel();
        this.jLabel61 = new JLabel();
        this.jLabel62 = new JLabel();
        this.jLabel63 = new JLabel();
        this.jLabel64 = new JLabel();
        this.jLabel65 = new JLabel();
        this.jLabel66 = new JLabel();
        this.txtId = new JTextField();
        this.jLabel1 = new JLabel();
        this.jLabel2 = new JLabel();
        this.btnExitApplication = new JButton();
        this.btnConfirm = new JButton();
        this.setTitle("Validasi Sheet 214021300");
        this.setResizable(false);
        this.jLabel3.setFont(new Font("Tahoma", 1, 11));
        this.jLabel3.setText("ID :");
        this.jLabel4.setFont(new Font("Tahoma", 1, 11));
        this.jLabel4.setText("A. No :");
        this.jLabel5.setFont(new Font("Tahoma", 1, 11));
        this.jLabel5.setText("B. No Kontrak");
        this.jLabel7.setFont(new Font("Tahoma", 1, 11));
        this.jLabel7.setText("B. Tanggal Mulai Pembiayaan :");
        this.jLabel8.setFont(new Font("Tahoma", 1, 11));
        this.jLabel8.setText("C. Tanggal Jatuh Tempo :");
        this.jLabel9.setFont(new Font("Tahoma", 1, 11));
        this.jLabel9.setText("D. Jenis Bagi Hasil :");
        this.jLabel10.setFont(new Font("Tahoma", 1, 11));
        this.jLabel10.setText("E. Tingkat Bagi Hasil :");
        this.jLabel11.setFont(new Font("Tahoma", 1, 11));
        this.jLabel11.setText("F. Nilai Awal Pembiayaan :");
        this.jLabel12.setFont(new Font("Tahoma", 1, 11));
        this.jLabel12.setText("G. Kualitas :");
        this.jLabel13.setFont(new Font("Tahoma", 1, 11));
        this.jLabel13.setText("H. Jenis Valuta :");
        this.jLabel14.setFont(new Font("Tahoma", 1, 11));
        this.jLabel14.setText("I. Tagihan Piutang Pembiayaan Bruto Dalam Mata Uang Asal :");
        this.jLabel15.setFont(new Font("Tahoma", 1, 11));
        this.jLabel15.setText("J. Tagihan Piutang Pembiayaan Bruto Dalam Equivalen Rupiah :");
        this.jLabel16.setFont(new Font("Tahoma", 1, 11));
        this.jLabel16.setText("K. Bunga / Bagi Hasil Yang Di Tangguhkan Dalam Mata Uang Asal :");
        this.jLabel17.setFont(new Font("Tahoma", 1, 11));
        this.jLabel17.setText("L. Bunga / Bagi Hasil Yang Di Tangguhkan Dalam Equivalen Rupiah :");
        this.jLabel18.setFont(new Font("Tahoma", 1, 11));
        this.jLabel18.setText("M. Piutang Pembiayaan Pokok Dalam Mata Uang Asal :");
        this.jLabel19.setFont(new Font("Tahoma", 1, 11));
        this.jLabel19.setText("N. Piutang Pembiayaan Pokok Dalam Equivalen Rupiah :");
        this.jLabel20.setFont(new Font("Tahoma", 1, 11));
        this.jLabel20.setText("O. Metode Pembentukan Cadangan Kerugian Penurunan Nilai:");
        this.jLabel21.setFont(new Font("Tahoma", 1, 11));
        this.jLabel21.setText("P. Nilai Aset Baik Cadangan Kerugian Penurunan Nilai :");
        this.jLabel22.setFont(new Font("Tahoma", 1, 11));
        this.jLabel22.setText("Q. Nilai Aset Kurang Baik Cadangan Kerugian Penurunan Nilai :");
        this.jLabel23.setFont(new Font("Tahoma", 1, 11));
        this.jLabel23.setText("R. Nilai Aset Tidak Baik Cadangan Kerugian Penurunan Nilai :");
        this.jLabel24.setFont(new Font("Tahoma", 1, 11));
        this.jLabel24.setText("S. Proporsi Penjaminan Pembiayaan :");
        this.jLabel26.setFont(new Font("Tahoma", 1, 11));
        this.jLabel26.setText("V. Bentuk Pasangan Usaha / Debitur :");
        this.jLabel27.setFont(new Font("Tahoma", 1, 11));
        this.jLabel27.setText("W. Kategori Usaha Pasangan Usaha / Debitur :");
        this.jLabel28.setFont(new Font("Tahoma", 1, 11));
        this.jLabel28.setText("X. Kategori Usaha Keuangan Berkelanjutan :");
        this.jLabel29.setFont(new Font("Tahoma", 1, 11));
        this.jLabel29.setText("Y. Golongan Pasangan Usaha / Debitur :");
        this.jLabel30.setFont(new Font("Tahoma", 1, 11));
        this.jLabel30.setText("Z. Status Keterkaitan :");
        this.jLabel31.setFont(new Font("Tahoma", 1, 11));
        this.jLabel31.setText("AA. Lokasi Kabupaten Kota :");
        this.jLabel32.setFont(new Font("Tahoma", 1, 11));
        this.jLabel32.setText("AB. Sektor Eknomi :");
        this.jLabel33.setFont(new Font("Tahoma", 1, 11));
        this.jLabel33.setText("AC. Nomor Agunan :");
        this.jLabel34.setFont(new Font("Tahoma", 1, 11));
        this.jLabel34.setText("AD. Jenis Agunan :");
        this.jLabel35.setFont(new Font("Tahoma", 1, 11));
        this.jLabel35.setText("AE. Nilai Agunan :");
        this.jLabel25.setFont(new Font("Tahoma", 1, 11));
        this.jLabel25.setText("T. Nama Pasangan Usaha / Debitur :");
        this.txtNo.setEditable(false);
        this.lblNoKontrak.setFont(new Font("Tahoma", 0, 9));
        this.lblNoKontrak.setForeground(new Color(255, 0, 0));
        this.lblNoKontrak.setText("-");
        this.lblTanggalMulaiPembiayaan.setFont(new Font("Tahoma", 0, 9));
        this.lblTanggalMulaiPembiayaan.setForeground(new Color(255, 0, 0));
        this.lblTanggalMulaiPembiayaan.setText("-");
        this.lblTanggalJatuhTempo.setFont(new Font("Tahoma", 0, 9));
        this.lblTanggalJatuhTempo.setForeground(new Color(255, 0, 0));
        this.lblTanggalJatuhTempo.setText("-");
        this.lblJenisBagiHasil.setFont(new Font("Tahoma", 0, 9));
        this.lblJenisBagiHasil.setForeground(new Color(255, 0, 0));
        this.lblJenisBagiHasil.setText("-");
        this.lblTingkatBagiHasil.setFont(new Font("Tahoma", 0, 9));
        this.lblTingkatBagiHasil.setForeground(new Color(255, 0, 0));
        this.lblTingkatBagiHasil.setText("-");
        this.lblNilaiAwalPembiayaan.setFont(new Font("Tahoma", 0, 9));
        this.lblNilaiAwalPembiayaan.setForeground(new Color(255, 0, 0));
        this.lblNilaiAwalPembiayaan.setText("-");
        this.lblKualitas.setFont(new Font("Tahoma", 0, 9));
        this.lblKualitas.setForeground(new Color(255, 0, 0));
        this.lblKualitas.setText("-");
        this.lblJenisValuta.setFont(new Font("Tahoma", 0, 9));
        this.lblJenisValuta.setForeground(new Color(255, 0, 0));
        this.lblJenisValuta.setText("-");
        this.lblTagihanPiutangPembiayaanBrutoDalamMataUangAsal.setFont(new Font("Tahoma", 0, 9));
        this.lblTagihanPiutangPembiayaanBrutoDalamMataUangAsal.setForeground(new Color(255, 0, 0));
        this.lblTagihanPiutangPembiayaanBrutoDalamMataUangAsal.setText("-");
        this.lblTagihanPiutangPembiayaanBrutoDalamEquivalenRupiah.setFont(new Font("Tahoma", 0, 9));
        this.lblTagihanPiutangPembiayaanBrutoDalamEquivalenRupiah.setForeground(new Color(255, 0, 0));
        this.lblTagihanPiutangPembiayaanBrutoDalamEquivalenRupiah.setText("-");
        this.lblBungaBagiHasilYangDiTangguhkanDalamMataUangAsal.setFont(new Font("Tahoma", 0, 9));
        this.lblBungaBagiHasilYangDiTangguhkanDalamMataUangAsal.setForeground(new Color(255, 0, 0));
        this.lblBungaBagiHasilYangDiTangguhkanDalamMataUangAsal.setText("-");
        this.lblBungaBagiHasilYangDiTangguhkanDalamEquivalenRupiah.setFont(new Font("Tahoma", 0, 9));
        this.lblBungaBagiHasilYangDiTangguhkanDalamEquivalenRupiah.setForeground(new Color(255, 0, 0));
        this.lblBungaBagiHasilYangDiTangguhkanDalamEquivalenRupiah.setText("-");
        this.lblPiutangPembiayaanPokokDalamMataUangAsal.setFont(new Font("Tahoma", 0, 9));
        this.lblPiutangPembiayaanPokokDalamMataUangAsal.setForeground(new Color(255, 0, 0));
        this.lblPiutangPembiayaanPokokDalamMataUangAsal.setText("-");
        this.lblPiutangPembiayaanPokokDalamEquivalenRupiah.setFont(new Font("Tahoma", 0, 9));
        this.lblPiutangPembiayaanPokokDalamEquivalenRupiah.setForeground(new Color(255, 0, 0));
        this.lblPiutangPembiayaanPokokDalamEquivalenRupiah.setText("-");
        this.lblMetodePembentukanCadanganKerugian.setFont(new Font("Tahoma", 0, 9));
        this.lblMetodePembentukanCadanganKerugian.setForeground(new Color(255, 0, 0));
        this.lblMetodePembentukanCadanganKerugian.setText("-");
        this.lblNilaiAsetBaikCadanganKerugianPenurunanNilai.setFont(new Font("Tahoma", 0, 9));
        this.lblNilaiAsetBaikCadanganKerugianPenurunanNilai.setForeground(new Color(255, 0, 0));
        this.lblNilaiAsetBaikCadanganKerugianPenurunanNilai.setText("-");
        this.lblNilaiAsetKurangBaikCadanganKerugianPenurunanNilai.setFont(new Font("Tahoma", 0, 9));
        this.lblNilaiAsetKurangBaikCadanganKerugianPenurunanNilai.setForeground(new Color(255, 0, 0));
        this.lblNilaiAsetKurangBaikCadanganKerugianPenurunanNilai.setText("-");
        this.lblNilaiAsetTidakBaikCadanganKerugianPenurunanNilai.setFont(new Font("Tahoma", 0, 9));
        this.lblNilaiAsetTidakBaikCadanganKerugianPenurunanNilai.setForeground(new Color(255, 0, 0));
        this.lblNilaiAsetTidakBaikCadanganKerugianPenurunanNilai.setText("-");
        this.lblProporsiPenjaminanPembiayaan.setFont(new Font("Tahoma", 0, 9));
        this.lblProporsiPenjaminanPembiayaan.setForeground(new Color(255, 0, 0));
        this.lblProporsiPenjaminanPembiayaan.setText("-");
        this.lblNamaPasanganUsahaDebitur.setFont(new Font("Tahoma", 0, 9));
        this.lblNamaPasanganUsahaDebitur.setForeground(new Color(255, 0, 0));
        this.lblNamaPasanganUsahaDebitur.setText("-");
        this.lblBentukPasanganUsahaDebitur.setFont(new Font("Tahoma", 0, 9));
        this.lblBentukPasanganUsahaDebitur.setForeground(new Color(255, 0, 0));
        this.lblBentukPasanganUsahaDebitur.setText("-");
        this.lblKategoriUsahaPasanganDebitur.setFont(new Font("Tahoma", 0, 9));
        this.lblKategoriUsahaPasanganDebitur.setForeground(new Color(255, 0, 0));
        this.lblKategoriUsahaPasanganDebitur.setText("-");
        this.lblKategoriUsahaKeuanganBerkelanjutan.setFont(new Font("Tahoma", 0, 9));
        this.lblKategoriUsahaKeuanganBerkelanjutan.setForeground(new Color(255, 0, 0));
        this.lblKategoriUsahaKeuanganBerkelanjutan.setText("-");
        this.lblGolonganPasanganUsahaDebitur.setFont(new Font("Tahoma", 0, 9));
        this.lblGolonganPasanganUsahaDebitur.setForeground(new Color(255, 0, 0));
        this.lblGolonganPasanganUsahaDebitur.setText("-");
        this.lblStatusKeterkaitan.setFont(new Font("Tahoma", 0, 9));
        this.lblStatusKeterkaitan.setForeground(new Color(255, 0, 0));
        this.lblStatusKeterkaitan.setText("-");
        this.lblLokasiKabupatenKota.setFont(new Font("Tahoma", 0, 9));
        this.lblLokasiKabupatenKota.setForeground(new Color(255, 0, 0));
        this.lblLokasiKabupatenKota.setText("-");
        this.lblSektorEknomi.setFont(new Font("Tahoma", 0, 9));
        this.lblSektorEknomi.setForeground(new Color(255, 0, 0));
        this.lblSektorEknomi.setText("-");
        this.lblNomorAgunan.setFont(new Font("Tahoma", 0, 9));
        this.lblNomorAgunan.setForeground(new Color(255, 0, 0));
        this.lblNomorAgunan.setText("-");
        this.lblJenisAgunan.setFont(new Font("Tahoma", 0, 9));
        this.lblJenisAgunan.setForeground(new Color(255, 0, 0));
        this.lblJenisAgunan.setText("-");
        this.lblNilaiAgunan.setFont(new Font("Tahoma", 0, 9));
        this.lblNilaiAgunan.setForeground(new Color(255, 0, 0));
        this.lblNilaiAgunan.setText("-");
        this.jLabel36.setFont(new Font("Tahoma", 0, 10));
        this.jLabel36.setForeground(new Color(102, 102, 102));
        this.jLabel36.setText("text");
        this.jLabel38.setFont(new Font("Tahoma", 0, 10));
        this.jLabel38.setForeground(new Color(102, 102, 102));
        this.jLabel38.setText("date (yyyy-mm-dd)");
        this.jLabel39.setFont(new Font("Tahoma", 0, 10));
        this.jLabel39.setForeground(new Color(102, 102, 102));
        this.jLabel39.setText("date (yyyy-mm-dd)");
        this.jLabel40.setFont(new Font("Tahoma", 0, 10));
        this.jLabel40.setForeground(new Color(102, 102, 102));
        this.jLabel40.setText("text");
        this.jLabel41.setFont(new Font("Tahoma", 0, 10));
        this.jLabel41.setForeground(new Color(102, 102, 102));
        this.jLabel41.setText("decimal");
        this.jLabel42.setFont(new Font("Tahoma", 0, 10));
        this.jLabel42.setForeground(new Color(102, 102, 102));
        this.jLabel42.setText("monetary");
        this.jLabel43.setFont(new Font("Tahoma", 0, 10));
        this.jLabel43.setForeground(new Color(102, 102, 102));
        this.jLabel43.setText("text");
        this.jLabel44.setFont(new Font("Tahoma", 0, 10));
        this.jLabel44.setForeground(new Color(102, 102, 102));
        this.jLabel44.setText("text");
        this.jLabel45.setFont(new Font("Tahoma", 0, 10));
        this.jLabel45.setForeground(new Color(102, 102, 102));
        this.jLabel45.setText("monetary");
        this.jLabel46.setFont(new Font("Tahoma", 0, 10));
        this.jLabel46.setForeground(new Color(102, 102, 102));
        this.jLabel46.setText("monetary");
        this.jLabel47.setFont(new Font("Tahoma", 0, 10));
        this.jLabel47.setForeground(new Color(102, 102, 102));
        this.jLabel47.setText("monetary");
        this.jLabel48.setFont(new Font("Tahoma", 0, 10));
        this.jLabel48.setForeground(new Color(102, 102, 102));
        this.jLabel48.setText("monetary");
        this.jLabel49.setFont(new Font("Tahoma", 0, 10));
        this.jLabel49.setForeground(new Color(102, 102, 102));
        this.jLabel49.setText("monetary");
        this.jLabel50.setFont(new Font("Tahoma", 0, 10));
        this.jLabel50.setForeground(new Color(102, 102, 102));
        this.jLabel50.setText("monetary");
        this.jLabel51.setFont(new Font("Tahoma", 0, 10));
        this.jLabel51.setForeground(new Color(102, 102, 102));
        this.jLabel51.setText("text");
        this.jLabel52.setFont(new Font("Tahoma", 0, 10));
        this.jLabel52.setForeground(new Color(102, 102, 102));
        this.jLabel52.setText("monetary");
        this.jLabel53.setFont(new Font("Tahoma", 0, 10));
        this.jLabel53.setForeground(new Color(102, 102, 102));
        this.jLabel53.setText("monetary");
        this.jLabel54.setFont(new Font("Tahoma", 0, 10));
        this.jLabel54.setForeground(new Color(102, 102, 102));
        this.jLabel54.setText("monetary");
        this.jLabel55.setFont(new Font("Tahoma", 0, 10));
        this.jLabel55.setForeground(new Color(102, 102, 102));
        this.jLabel55.setText("decimal");
        this.jLabel56.setFont(new Font("Tahoma", 0, 10));
        this.jLabel56.setForeground(new Color(102, 102, 102));
        this.jLabel56.setText("text");
        this.jLabel57.setFont(new Font("Tahoma", 0, 10));
        this.jLabel57.setForeground(new Color(102, 102, 102));
        this.jLabel57.setText("text");
        this.jLabel58.setFont(new Font("Tahoma", 0, 10));
        this.jLabel58.setForeground(new Color(102, 102, 102));
        this.jLabel58.setText("text");
        this.jLabel59.setFont(new Font("Tahoma", 0, 10));
        this.jLabel59.setForeground(new Color(102, 102, 102));
        this.jLabel59.setText("text");
        this.jLabel60.setFont(new Font("Tahoma", 0, 10));
        this.jLabel60.setForeground(new Color(102, 102, 102));
        this.jLabel60.setText("text");
        this.jLabel61.setFont(new Font("Tahoma", 0, 10));
        this.jLabel61.setForeground(new Color(102, 102, 102));
        this.jLabel61.setText("text");
        this.jLabel62.setFont(new Font("Tahoma", 0, 10));
        this.jLabel62.setForeground(new Color(102, 102, 102));
        this.jLabel62.setText("text");
        this.jLabel63.setFont(new Font("Tahoma", 0, 10));
        this.jLabel63.setForeground(new Color(102, 102, 102));
        this.jLabel63.setText("text");
        this.jLabel64.setFont(new Font("Tahoma", 0, 10));
        this.jLabel64.setForeground(new Color(102, 102, 102));
        this.jLabel64.setText("text");
        this.jLabel65.setFont(new Font("Tahoma", 0, 10));
        this.jLabel65.setForeground(new Color(102, 102, 102));
        this.jLabel65.setText("text");
        this.jLabel66.setFont(new Font("Tahoma", 0, 10));
        this.jLabel66.setForeground(new Color(102, 102, 102));
        this.jLabel66.setText("monetary");
        this.txtId.setEditable(false);
        this.txtId.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent evt) {
                DialogValidateS214021350.this.txtIdActionPerformed(evt);
            }
        });
        final GroupLayout jPanel1Layout = new GroupLayout(this.jPanel1);
        this.jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup().addContainerGap().addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.jLabel4).addComponent(this.jLabel3).addGroup(jPanel1Layout.createSequentialGroup().addComponent(this.jLabel5).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jLabel36)).addGroup(jPanel1Layout.createSequentialGroup().addComponent(this.jLabel7).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jLabel38)).addGroup(jPanel1Layout.createSequentialGroup().addComponent(this.jLabel8).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jLabel39)).addGroup(jPanel1Layout.createSequentialGroup().addComponent(this.jLabel9).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jLabel40)).addGroup(jPanel1Layout.createSequentialGroup().addComponent(this.jLabel10).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jLabel41)).addGroup(jPanel1Layout.createSequentialGroup().addComponent(this.jLabel11).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jLabel42)).addGroup(jPanel1Layout.createSequentialGroup().addComponent(this.jLabel12).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jLabel43)).addGroup(jPanel1Layout.createSequentialGroup().addComponent(this.jLabel13).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jLabel44)).addGroup(jPanel1Layout.createSequentialGroup().addComponent(this.jLabel15).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jLabel46)).addGroup(jPanel1Layout.createSequentialGroup().addComponent(this.jLabel17).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jLabel48)).addGroup(jPanel1Layout.createSequentialGroup().addComponent(this.jLabel18).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jLabel49)).addGroup(jPanel1Layout.createSequentialGroup().addComponent(this.jLabel19).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jLabel50)).addGroup(jPanel1Layout.createSequentialGroup().addComponent(this.jLabel20).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jLabel51)).addGroup(jPanel1Layout.createSequentialGroup().addComponent(this.jLabel21).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jLabel52)).addGroup(jPanel1Layout.createSequentialGroup().addComponent(this.jLabel22).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jLabel53)).addGroup(jPanel1Layout.createSequentialGroup().addComponent(this.jLabel23).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jLabel54)).addGroup(jPanel1Layout.createSequentialGroup().addComponent(this.jLabel24).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jLabel55)).addGroup(jPanel1Layout.createSequentialGroup().addComponent(this.jLabel25).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jLabel56)).addGroup(jPanel1Layout.createSequentialGroup().addComponent(this.jLabel26).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jLabel57)).addGroup(jPanel1Layout.createSequentialGroup().addComponent(this.jLabel27).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jLabel58)).addGroup(jPanel1Layout.createSequentialGroup().addComponent(this.jLabel28).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jLabel59)).addGroup(jPanel1Layout.createSequentialGroup().addComponent(this.jLabel29).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jLabel60)).addGroup(jPanel1Layout.createSequentialGroup().addComponent(this.jLabel30).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jLabel61)).addGroup(jPanel1Layout.createSequentialGroup().addComponent(this.jLabel31).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jLabel62)).addGroup(jPanel1Layout.createSequentialGroup().addComponent(this.jLabel32).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jLabel63)).addGroup(jPanel1Layout.createSequentialGroup().addComponent(this.jLabel33).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jLabel64)).addGroup(jPanel1Layout.createSequentialGroup().addComponent(this.jLabel34).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jLabel65)).addGroup(jPanel1Layout.createSequentialGroup().addComponent(this.jLabel35).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jLabel66)).addGroup(jPanel1Layout.createSequentialGroup().addComponent(this.jLabel14).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jLabel45)).addGroup(jPanel1Layout.createSequentialGroup().addComponent(this.jLabel16).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jLabel47))).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED, 54, 32767).addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING, false).addComponent(this.txtNoKontrak, -1, 350, 32767).addComponent(this.txtTanggalMulaiPembiayaan, -1, 350, 32767).addComponent(this.txtTanggalJatuhTempo, -1, 350, 32767).addComponent(this.txtJenisBagiHasil, -1, 350, 32767).addComponent(this.txtTingkatBagiHasil, -1, 350, 32767).addComponent(this.txtNilaiAwalPembiayaan, -1, 350, 32767).addComponent(this.txtKualitas, -1, 350, 32767).addComponent(this.txtJenisValuta, -1, 350, 32767).addComponent(this.txtTagihanPiutangPembiayaanBrutoDalamMataUangAsal, -1, 350, 32767).addComponent(this.txtTagihanPiutangPembiayaanBrutoDalamEquivalenRupiah, -1, 350, 32767).addComponent(this.txtBungaBagiHasilYangDiTangguhkanDalamMataUangAsal, -1, 350, 32767).addComponent(this.txtBungaBagiHasilYangDiTangguhkanDalamEquivalenRupiah, -1, 350, 32767).addComponent(this.txtPiutangPembiayaanPokokDalamMataUangAsal, -1, 350, 32767).addComponent(this.txtPiutangPembiayaanPokokDalamEquivalenRupiah, -1, 350, 32767).addComponent(this.txtMetodePembentukanCadanganKerugian, -1, 350, 32767).addComponent(this.txtNilaiAsetBaikCadanganKerugianPenurunanNilai, -1, 350, 32767).addComponent(this.txtNilaiAsetKurangBaikCadanganKerugianPenurunanNilai, -1, 350, 32767).addComponent(this.txtNilaiAsetTidakBaikCadanganKerugianPenurunanNilai, -1, 350, 32767).addComponent(this.txtProporsiPenjaminanPembiayaan, GroupLayout.Alignment.TRAILING, -1, 350, 32767).addComponent(this.txtNamaPasanganUsahaDebitur, -1, 350, 32767).addComponent(this.txtBentukPasanganUsahaDebitur, -1, 350, 32767).addComponent(this.txtKategoriUsahaPasanganDebitur, -1, 350, 32767).addComponent(this.txtKategoriUsahaKeuanganBerkelanjutan, GroupLayout.Alignment.TRAILING, -1, 350, 32767).addComponent(this.txtGolonganPasanganUsahaDebitur, GroupLayout.Alignment.TRAILING, -1, 350, 32767).addComponent(this.txtStatusKeterkaitan, GroupLayout.Alignment.TRAILING, -1, 350, 32767).addComponent(this.txtLokasiKabupatenKota, -1, 350, 32767).addComponent(this.txtSektorEknomi, -1, 350, 32767).addComponent(this.txtNomorAgunan, -1, 350, 32767).addComponent(this.txtJenisAgunan, -1, 350, 32767).addComponent(this.txtNilaiAgunan, -1, 350, 32767).addComponent(this.lblTanggalMulaiPembiayaan, -1, 350, 32767).addComponent(this.lblTanggalJatuhTempo, -1, 350, 32767).addComponent(this.lblJenisBagiHasil, -1, 350, 32767).addComponent(this.lblTingkatBagiHasil, GroupLayout.Alignment.TRAILING, -1, 350, 32767).addComponent(this.lblNilaiAwalPembiayaan, -1, 350, 32767).addComponent(this.lblKualitas, -1, 350, 32767).addComponent(this.lblJenisValuta, -1, 350, 32767).addComponent(this.lblTagihanPiutangPembiayaanBrutoDalamMataUangAsal, -1, 350, 32767).addComponent(this.lblTagihanPiutangPembiayaanBrutoDalamEquivalenRupiah, GroupLayout.Alignment.TRAILING, -1, 350, 32767).addComponent(this.lblBungaBagiHasilYangDiTangguhkanDalamMataUangAsal, -1, 350, 32767).addComponent(this.lblBungaBagiHasilYangDiTangguhkanDalamEquivalenRupiah, GroupLayout.Alignment.TRAILING, -1, 350, 32767).addComponent(this.lblPiutangPembiayaanPokokDalamMataUangAsal, -1, 350, 32767).addComponent(this.lblPiutangPembiayaanPokokDalamEquivalenRupiah, GroupLayout.Alignment.TRAILING, -1, 350, 32767).addComponent(this.lblMetodePembentukanCadanganKerugian, GroupLayout.Alignment.TRAILING, -1, 350, 32767).addComponent(this.lblNilaiAsetBaikCadanganKerugianPenurunanNilai, GroupLayout.Alignment.TRAILING, -1, 350, 32767).addComponent(this.lblNilaiAsetKurangBaikCadanganKerugianPenurunanNilai, GroupLayout.Alignment.TRAILING, -1, 350, 32767).addComponent(this.lblNilaiAsetTidakBaikCadanganKerugianPenurunanNilai, GroupLayout.Alignment.TRAILING, -1, 350, 32767).addComponent(this.lblProporsiPenjaminanPembiayaan, -1, 350, 32767).addComponent(this.lblNamaPasanganUsahaDebitur, -1, 350, 32767).addComponent(this.lblBentukPasanganUsahaDebitur, -1, 350, 32767).addComponent(this.lblKategoriUsahaPasanganDebitur, -1, 350, 32767).addComponent(this.lblKategoriUsahaKeuanganBerkelanjutan, -1, 350, 32767).addComponent(this.lblGolonganPasanganUsahaDebitur, -1, 350, 32767).addComponent(this.lblStatusKeterkaitan, GroupLayout.Alignment.TRAILING, -1, 350, 32767).addComponent(this.lblLokasiKabupatenKota, GroupLayout.Alignment.TRAILING, -1, 350, 32767).addComponent(this.lblSektorEknomi, GroupLayout.Alignment.TRAILING, -1, 350, 32767).addComponent(this.lblNomorAgunan, GroupLayout.Alignment.TRAILING, -1, 350, 32767).addComponent(this.lblJenisAgunan, -1, 350, 32767).addComponent(this.lblNilaiAgunan, -1, 350, 32767).addComponent(this.txtNo, -2, 89, -2).addComponent(this.txtId, -2, 138, -2).addComponent(this.lblNoKontrak, -1, 350, 32767)).addContainerGap()));
        jPanel1Layout.setVerticalGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(jPanel1Layout.createSequentialGroup().addContainerGap().addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel3).addComponent(this.txtId, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel4).addComponent(this.txtNo, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel5).addComponent(this.jLabel36)).addComponent(this.txtNoKontrak, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.lblNoKontrak).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel7).addComponent(this.jLabel38)).addGroup(jPanel1Layout.createSequentialGroup().addComponent(this.txtTanggalMulaiPembiayaan, -2, -1, -2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.lblTanggalMulaiPembiayaan))).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel8).addComponent(this.jLabel39)).addGroup(jPanel1Layout.createSequentialGroup().addComponent(this.txtTanggalJatuhTempo, -2, -1, -2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.lblTanggalJatuhTempo))).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel9).addComponent(this.jLabel40)).addGroup(jPanel1Layout.createSequentialGroup().addComponent(this.txtJenisBagiHasil, -2, -1, -2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.lblJenisBagiHasil))).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel10).addComponent(this.jLabel41)).addGroup(jPanel1Layout.createSequentialGroup().addComponent(this.txtTingkatBagiHasil, -2, -1, -2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.lblTingkatBagiHasil))).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel11).addComponent(this.txtNilaiAwalPembiayaan, -2, -1, -2).addComponent(this.jLabel42)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.lblNilaiAwalPembiayaan).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel12).addComponent(this.txtKualitas, -2, -1, -2).addComponent(this.jLabel43)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.lblKualitas).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel13).addComponent(this.txtJenisValuta, -2, -1, -2).addComponent(this.jLabel44)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.lblJenisValuta).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel14).addComponent(this.txtTagihanPiutangPembiayaanBrutoDalamMataUangAsal, -2, -1, -2).addComponent(this.jLabel45)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.lblTagihanPiutangPembiayaanBrutoDalamMataUangAsal).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel15).addComponent(this.txtTagihanPiutangPembiayaanBrutoDalamEquivalenRupiah, -2, -1, -2).addComponent(this.jLabel46)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.lblTagihanPiutangPembiayaanBrutoDalamEquivalenRupiah).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel16).addComponent(this.txtBungaBagiHasilYangDiTangguhkanDalamMataUangAsal, -2, -1, -2).addComponent(this.jLabel47)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.lblBungaBagiHasilYangDiTangguhkanDalamMataUangAsal).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel17).addComponent(this.txtBungaBagiHasilYangDiTangguhkanDalamEquivalenRupiah, -2, -1, -2).addComponent(this.jLabel48)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.lblBungaBagiHasilYangDiTangguhkanDalamEquivalenRupiah).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel18).addComponent(this.txtPiutangPembiayaanPokokDalamMataUangAsal, -2, -1, -2).addComponent(this.jLabel49)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.lblPiutangPembiayaanPokokDalamMataUangAsal).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel19).addComponent(this.txtPiutangPembiayaanPokokDalamEquivalenRupiah, -2, -1, -2).addComponent(this.jLabel50)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.lblPiutangPembiayaanPokokDalamEquivalenRupiah).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel20).addComponent(this.txtMetodePembentukanCadanganKerugian, -2, -1, -2).addComponent(this.jLabel51)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.lblMetodePembentukanCadanganKerugian).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel21).addComponent(this.txtNilaiAsetBaikCadanganKerugianPenurunanNilai, -2, -1, -2).addComponent(this.jLabel52)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.lblNilaiAsetBaikCadanganKerugianPenurunanNilai).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel22).addComponent(this.txtNilaiAsetKurangBaikCadanganKerugianPenurunanNilai, -2, -1, -2).addComponent(this.jLabel53)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.lblNilaiAsetKurangBaikCadanganKerugianPenurunanNilai).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel23).addComponent(this.txtNilaiAsetTidakBaikCadanganKerugianPenurunanNilai, -2, -1, -2).addComponent(this.jLabel54)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.lblNilaiAsetTidakBaikCadanganKerugianPenurunanNilai).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel24).addComponent(this.txtProporsiPenjaminanPembiayaan, -2, -1, -2).addComponent(this.jLabel55)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.lblProporsiPenjaminanPembiayaan).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel25).addComponent(this.txtNamaPasanganUsahaDebitur, -2, -1, -2).addComponent(this.jLabel56)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.lblNamaPasanganUsahaDebitur).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel26).addComponent(this.txtBentukPasanganUsahaDebitur, -2, -1, -2).addComponent(this.jLabel57)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.lblBentukPasanganUsahaDebitur).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel27).addComponent(this.txtKategoriUsahaPasanganDebitur, -2, -1, -2).addComponent(this.jLabel58)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.lblKategoriUsahaPasanganDebitur).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel28).addComponent(this.txtKategoriUsahaKeuanganBerkelanjutan, -2, -1, -2).addComponent(this.jLabel59)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.lblKategoriUsahaKeuanganBerkelanjutan).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel29).addComponent(this.txtGolonganPasanganUsahaDebitur, -2, -1, -2).addComponent(this.jLabel60)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.lblGolonganPasanganUsahaDebitur).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel30).addComponent(this.txtStatusKeterkaitan, -2, -1, -2).addComponent(this.jLabel61)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.lblStatusKeterkaitan).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel31).addComponent(this.txtLokasiKabupatenKota, -2, -1, -2).addComponent(this.jLabel62)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.lblLokasiKabupatenKota).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel32).addComponent(this.txtSektorEknomi, -2, -1, -2).addComponent(this.jLabel63)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.lblSektorEknomi).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel33).addComponent(this.txtNomorAgunan, -2, -1, -2).addComponent(this.jLabel64)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.lblNomorAgunan).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel34).addComponent(this.txtJenisAgunan, -2, -1, -2).addComponent(this.jLabel65)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.lblJenisAgunan).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel35).addComponent(this.txtNilaiAgunan, -2, -1, -2).addComponent(this.jLabel66)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.lblNilaiAgunan).addContainerGap(-1, 32767)));
        this.jScrollPane1.setViewportView(this.jPanel1);
        this.jLabel1.setFont(new Font("Tahoma", 1, 11));
        this.jLabel1.setText("Validasi Sheet 214021300");
        this.jLabel2.setForeground(new Color(255, 0, 0));
        this.jLabel2.setText("*Harap perbaiki input di bawah ini");
        this.btnExitApplication.setText("Exit Aplication");
        this.btnExitApplication.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent evt) {
                DialogValidateS214021350.this.btnExitApplicationActionPerformed(evt);
            }
        });
        this.btnConfirm.setText("Confirm");
        this.btnConfirm.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent evt) {
                DialogValidateS214021350.this.btnConfirmActionPerformed(evt);
            }
        });
        final GroupLayout layout = new GroupLayout(this.getContentPane());
        this.getContentPane().setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup().addContainerGap(-1, 32767).addComponent(this.btnExitApplication).addGap(129, 129, 129)).addGroup(layout.createSequentialGroup().addGap(33, 33, 33).addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING).addComponent(this.btnConfirm).addGroup(layout.createSequentialGroup().addComponent(this.jLabel2).addGap(646, 646, 646)).addComponent(this.jLabel1)).addContainerGap(27, 32767)).addGroup(layout.createSequentialGroup().addComponent(this.jScrollPane1).addContainerGap()));
        layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addContainerGap().addComponent(this.jLabel1).addGap(1, 1, 1).addComponent(this.jLabel2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jScrollPane1, -2, 550, -2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.btnExitApplication).addComponent(this.btnConfirm)).addContainerGap(-1, 32767)));
        this.pack();
    }
    
    private void btnExitApplicationActionPerformed(final ActionEvent evt) {
        final int dialogButton = 0;
        final int dialogResult = JOptionPane.showConfirmDialog(null, "Anda yakin ingin keluar dari aplikasi?", "Warning", dialogButton);
        if (dialogResult == 0) {
            System.exit(0);
        }
    }
    
    private void btnConfirmActionPerformed(final ActionEvent evt) {
        if (this.isValidateMonetary) {
            final int dialogButton = 0;
            final int dialogResult = JOptionPane.showConfirmDialog(null, "Ubah semua nilai pada kolom dengan type monetary pada setiap row ke nilai monetary jika terdapat nilai bukan monetary?", "Info", dialogButton);
            if (dialogResult == 0) {
                this.castAllRowToMonetary = true;
            }
        }
        this.dispose();
    }
    
    private void txtIdActionPerformed(final ActionEvent evt) {
    }
    
    public static void main(final String[] args) {
        try {
            for (final UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        }
        catch (ClassNotFoundException ex) {
            Logger.getLogger(DialogValidateS214021350.class.getName()).log(Level.SEVERE, null, ex);
        }
        catch (InstantiationException ex2) {
            Logger.getLogger(DialogValidateS214021350.class.getName()).log(Level.SEVERE, null, ex2);
        }
        catch (IllegalAccessException ex3) {
            Logger.getLogger(DialogValidateS214021350.class.getName()).log(Level.SEVERE, null, ex3);
        }
        catch (UnsupportedLookAndFeelException ex4) {
            Logger.getLogger(DialogValidateS214021350.class.getName()).log(Level.SEVERE, null, ex4);
        }
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                final DialogValidateS214021350 dialog = new DialogValidateS214021350(new JFrame(), true);
                dialog.addWindowListener(new WindowAdapter() {
                    @Override
                    public void windowClosing(final WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }
}
