// 
// Decompiled by Procyon v0.5.36
// 

package view;

import java.io.File;
import javax.swing.filechooser.FileFilter;

public class ExtensionFileFilter extends FileFilter
{
    String description;
    String[] extensions;
    
    public ExtensionFileFilter(final String description, final String extension) {
        this(description, new String[] { extension });
    }
    
    public ExtensionFileFilter(final String description, final String[] extensions) {
        if (description == null) {
            this.description = extensions[0];
        }
        else {
            this.description = description;
        }
        this.toLower(this.extensions = extensions.clone());
    }
    
    private void toLower(final String[] array) {
        for (int i = 0, n = array.length; i < n; ++i) {
            array[i] = array[i].toLowerCase();
        }
    }
    
    @Override
    public String getDescription() {
        return this.description;
    }
    
    @Override
    public boolean accept(final File file) {
        if (file.isDirectory()) {
            return true;
        }
        final String path = file.getAbsolutePath().toLowerCase();
        for (int i = 0, n = this.extensions.length; i < n; ++i) {
            final String extension = this.extensions[i];
            if (path.endsWith(extension) && path.charAt(path.length() - extension.length() - 1) == '.') {
                return true;
            }
        }
        return false;
    }
}
