// 
// Decompiled by Procyon v0.5.36
// 

package model.sheet;

public class S214021300_TUP
{
    private String id;
    private String noKontrak;
    private String jenisPembiayaan;
    private String tanggalMulaiPembiayaan;
    private String tanggalJatuhTempo;
    private String jenisTingkatBungaBagiHasil;
    private String tingkatBungaBagiHasil;
    private String nilaiAwalPembiayaan;
    private String kualitas;
    private String jenisValuta;
    private String tagihanPiutangPembiayaanBrutoDalamMataUangAsal;
    private String tagihanPiutangPembiayaanBrutoDalamEquivalenRupiah;
    private String bungaBagiHasilYangDiTangguhkanDalamMataUangAsal;
    private String bungaBagiHasilYangDiTangguhkanDalamEquivalenRupiah;
    private String piutangPembiayaanPokokDalamMataUangAsal;
    private String piutangPembiayaanPokokDalamEquivalenRupiah;
    private String metodePembentukanCadanganKerugian;
    private String nilaiAsetBaikCadanganKerugianPenurunanNilai;
    private String nilaiAsetKurangBaikCadanganKerugianPenurunanNilai;
    private String nilaiAsetTidakBaikCadanganKerugianPenurunanNilai;
    private String proporsiPenjaminanPembiayaan;
    private String namaPasanganUsahaDebitur;
    private String bentukPasanganUsahaDebitur;
    private String kategoriUsahaPasanganDebitur;
    private String kategoriUsahaKeuanganBerkelanjutan;
    private String golonganPasanganUsahaDebitur;
    private String statusKeterkaitan;
    private String lokasiKabupatenKota;
    private String sektorEknomi;
    private F214021300_COL f214021300_col;
    
    public String getId() {
        return this.id;
    }
    
    public void setId(final String id) {
        this.id = id;
    }
    
    public String getNoKontrak() {
        return this.noKontrak;
    }
    
    public void setNoKontrak(final String noKontrak) {
        this.noKontrak = noKontrak;
    }
    
    public String getJenisPembiayaan() {
        return this.jenisPembiayaan;
    }
    
    public void setJenisPembiayaan(final String jenisPembiayaan) {
        this.jenisPembiayaan = jenisPembiayaan;
    }
    
    public String getTanggalMulaiPembiayaan() {
        return this.tanggalMulaiPembiayaan;
    }
    
    public void setTanggalMulaiPembiayaan(final String tanggalMulaiPembiayaan) {
        this.tanggalMulaiPembiayaan = tanggalMulaiPembiayaan;
    }
    
    public String getTanggalJatuhTempo() {
        return this.tanggalJatuhTempo;
    }
    
    public void setTanggalJatuhTempo(final String tanggalJatuhTempo) {
        this.tanggalJatuhTempo = tanggalJatuhTempo;
    }
    
    public String getJenisTingkatBungaBagiHasil() {
        return this.jenisTingkatBungaBagiHasil;
    }
    
    public void setJenisTingkatBungaBagiHasil(final String jenisTingkatBungaBagiHasil) {
        this.jenisTingkatBungaBagiHasil = jenisTingkatBungaBagiHasil;
    }
    
    public String getTingkatBungaBagiHasil() {
        return this.tingkatBungaBagiHasil;
    }
    
    public void setTingkatBungaBagiHasil(final String tingkatBungaBagiHasil) {
        this.tingkatBungaBagiHasil = tingkatBungaBagiHasil;
    }
    
    public String getNilaiAwalPembiayaan() {
        return this.nilaiAwalPembiayaan;
    }
    
    public void setNilaiAwalPembiayaan(final String nilaiAwalPembiayaan) {
        this.nilaiAwalPembiayaan = nilaiAwalPembiayaan;
    }
    
    public String getKualitas() {
        return this.kualitas;
    }
    
    public void setKualitas(final String kualitas) {
        this.kualitas = kualitas;
    }
    
    public String getJenisValuta() {
        return this.jenisValuta;
    }
    
    public void setJenisValuta(final String jenisValuta) {
        this.jenisValuta = jenisValuta;
    }
    
    public String getTagihanPiutangPembiayaanBrutoDalamMataUangAsal() {
        return this.tagihanPiutangPembiayaanBrutoDalamMataUangAsal;
    }
    
    public void setTagihanPiutangPembiayaanBrutoDalamMataUangAsal(final String tagihanPiutangPembiayaanBrutoDalamMataUangAsal) {
        this.tagihanPiutangPembiayaanBrutoDalamMataUangAsal = tagihanPiutangPembiayaanBrutoDalamMataUangAsal;
    }
    
    public String getTagihanPiutangPembiayaanBrutoDalamEquivalenRupiah() {
        return this.tagihanPiutangPembiayaanBrutoDalamEquivalenRupiah;
    }
    
    public void setTagihanPiutangPembiayaanBrutoDalamEquivalenRupiah(final String tagihanPiutangPembiayaanBrutoDalamEquivalenRupiah) {
        this.tagihanPiutangPembiayaanBrutoDalamEquivalenRupiah = tagihanPiutangPembiayaanBrutoDalamEquivalenRupiah;
    }
    
    public String getBungaBagiHasilYangDiTangguhkanDalamMataUangAsal() {
        return this.bungaBagiHasilYangDiTangguhkanDalamMataUangAsal;
    }
    
    public void setBungaBagiHasilYangDiTangguhkanDalamMataUangAsal(final String bungaBagiHasilYangDiTangguhkanDalamMataUangAsal) {
        this.bungaBagiHasilYangDiTangguhkanDalamMataUangAsal = bungaBagiHasilYangDiTangguhkanDalamMataUangAsal;
    }
    
    public String getBungaBagiHasilYangDiTangguhkanDalamEquivalenRupiah() {
        return this.bungaBagiHasilYangDiTangguhkanDalamEquivalenRupiah;
    }
    
    public void setBungaBagiHasilYangDiTangguhkanDalamEquivalenRupiah(final String bungaBagiHasilYangDiTangguhkanDalamEquivalenRupiah) {
        this.bungaBagiHasilYangDiTangguhkanDalamEquivalenRupiah = bungaBagiHasilYangDiTangguhkanDalamEquivalenRupiah;
    }
    
    public String getPiutangPembiayaanPokokDalamMataUangAsal() {
        return this.piutangPembiayaanPokokDalamMataUangAsal;
    }
    
    public void setPiutangPembiayaanPokokDalamMataUangAsal(final String piutangPembiayaanPokokDalamMataUangAsal) {
        this.piutangPembiayaanPokokDalamMataUangAsal = piutangPembiayaanPokokDalamMataUangAsal;
    }
    
    public String getPiutangPembiayaanPokokDalamEquivalenRupiah() {
        return this.piutangPembiayaanPokokDalamEquivalenRupiah;
    }
    
    public void setPiutangPembiayaanPokokDalamEquivalenRupiah(final String piutangPembiayaanPokokDalamEquivalenRupiah) {
        this.piutangPembiayaanPokokDalamEquivalenRupiah = piutangPembiayaanPokokDalamEquivalenRupiah;
    }
    
    public String getMetodePembentukanCadanganKerugian() {
        return this.metodePembentukanCadanganKerugian;
    }
    
    public void setMetodePembentukanCadanganKerugian(final String metodePembentukanCadanganKerugian) {
        this.metodePembentukanCadanganKerugian = metodePembentukanCadanganKerugian;
    }
    
    public String getNilaiAsetBaikCadanganKerugianPenurunanNilai() {
        return this.nilaiAsetBaikCadanganKerugianPenurunanNilai;
    }
    
    public void setNilaiAsetBaikCadanganKerugianPenurunanNilai(final String nilaiAsetBaikCadanganKerugianPenurunanNilai) {
        this.nilaiAsetBaikCadanganKerugianPenurunanNilai = nilaiAsetBaikCadanganKerugianPenurunanNilai;
    }
    
    public String getNilaiAsetKurangBaikCadanganKerugianPenurunanNilai() {
        return this.nilaiAsetKurangBaikCadanganKerugianPenurunanNilai;
    }
    
    public void setNilaiAsetKurangBaikCadanganKerugianPenurunanNilai(final String nilaiAsetKurangBaikCadanganKerugianPenurunanNilai) {
        this.nilaiAsetKurangBaikCadanganKerugianPenurunanNilai = nilaiAsetKurangBaikCadanganKerugianPenurunanNilai;
    }
    
    public String getNilaiAsetTidakBaikCadanganKerugianPenurunanNilai() {
        return this.nilaiAsetTidakBaikCadanganKerugianPenurunanNilai;
    }
    
    public void setNilaiAsetTidakBaikCadanganKerugianPenurunanNilai(final String nilaiAsetTidakBaikCadanganKerugianPenurunanNilai) {
        this.nilaiAsetTidakBaikCadanganKerugianPenurunanNilai = nilaiAsetTidakBaikCadanganKerugianPenurunanNilai;
    }
    
    public String getProporsiPenjaminanPembiayaan() {
        return this.proporsiPenjaminanPembiayaan;
    }
    
    public void setProporsiPenjaminanPembiayaan(final String proporsiPenjaminanPembiayaan) {
        this.proporsiPenjaminanPembiayaan = proporsiPenjaminanPembiayaan;
    }
    
    public String getNamaPasanganUsahaDebitur() {
        return this.namaPasanganUsahaDebitur;
    }
    
    public void setNamaPasanganUsahaDebitur(final String namaPasanganUsahaDebitur) {
        this.namaPasanganUsahaDebitur = namaPasanganUsahaDebitur;
    }
    
    public String getBentukPasanganUsahaDebitur() {
        return this.bentukPasanganUsahaDebitur;
    }
    
    public void setBentukPasanganUsahaDebitur(final String bentukPasanganUsahaDebitur) {
        this.bentukPasanganUsahaDebitur = bentukPasanganUsahaDebitur;
    }
    
    public String getKategoriUsahaPasanganDebitur() {
        return this.kategoriUsahaPasanganDebitur;
    }
    
    public void setKategoriUsahaPasanganDebitur(final String kategoriUsahaPasanganDebitur) {
        this.kategoriUsahaPasanganDebitur = kategoriUsahaPasanganDebitur;
    }
    
    public String getKategoriUsahaKeuanganBerkelanjutan() {
        return this.kategoriUsahaKeuanganBerkelanjutan;
    }
    
    public void setKategoriUsahaKeuanganBerkelanjutan(final String kategoriUsahaKeuanganBerkelanjutan) {
        this.kategoriUsahaKeuanganBerkelanjutan = kategoriUsahaKeuanganBerkelanjutan;
    }
    
    public String getGolonganPasanganUsahaDebitur() {
        return this.golonganPasanganUsahaDebitur;
    }
    
    public void setGolonganPasanganUsahaDebitur(final String golonganPasanganUsahaDebitur) {
        this.golonganPasanganUsahaDebitur = golonganPasanganUsahaDebitur;
    }
    
    public String getStatusKeterkaitan() {
        return this.statusKeterkaitan;
    }
    
    public void setStatusKeterkaitan(final String statusKeterkaitan) {
        this.statusKeterkaitan = statusKeterkaitan;
    }
    
    public String getLokasiKabupatenKota() {
        return this.lokasiKabupatenKota;
    }
    
    public void setLokasiKabupatenKota(final String lokasiKabupatenKota) {
        this.lokasiKabupatenKota = lokasiKabupatenKota;
    }
    
    public String getSektorEknomi() {
        return this.sektorEknomi;
    }
    
    public void setSektorEknomi(final String sektorEknomi) {
        this.sektorEknomi = sektorEknomi;
    }
    
    public F214021300_COL getF214021300_col() {
        return this.f214021300_col;
    }
    
    public void setF214021300_col(final F214021300_COL f214021300_col) {
        this.f214021300_col = f214021300_col;
    }
    
    public String buildStrXBRL() {
        String xbrl = "<F214021300:Fac id=\"" + this.id + "\">\n";
        if (this.noKontrak != null) {
            xbrl = xbrl + "\t\t<met:sd650 " + this.isNumber(this.noKontrak) + "contextRef=\"c\">" + this.noKontrak + "</met:sd650>\n";
        }
        if (this.jenisPembiayaan != null) {
            xbrl = xbrl + "\t\t<AK:ld6:sd650 " + this.isNumber(this.jenisPembiayaan) + "contextRef=\"c\">" + this.jenisPembiayaan + "</AK:ld6>\n";
        }
        if (this.tanggalMulaiPembiayaan != null) {
            xbrl = xbrl + "\t\t<met:dd56 " + this.isNumber(this.tanggalMulaiPembiayaan) + "contextRef=\"c\">" + this.tanggalMulaiPembiayaan + "</met:dd56>\n";
        }
        if (this.tanggalJatuhTempo != null) {
            xbrl = xbrl + "\t\t<met:dd53 " + this.isNumber(this.tanggalJatuhTempo) + "contextRef=\"c\">" + this.tanggalJatuhTempo + "</met:dd53>\n";
        }
        if (this.jenisTingkatBungaBagiHasil != null) {
            xbrl = xbrl + "\t\t<JK:ld16 " + this.isNumber(this.jenisTingkatBungaBagiHasil) + "contextRef=\"c\">" + this.jenisTingkatBungaBagiHasil + "</JK:ld16>\n";
        }
        if (this.tingkatBungaBagiHasil != null) {
            xbrl = xbrl + "\t\t<met:pd898 " + this.isNumber(this.tingkatBungaBagiHasil) + "contextRef=\"c\">" + this.tingkatBungaBagiHasil + "</met:pd898>\n";
        }
        if (this.nilaiAwalPembiayaan != null) {
            xbrl = xbrl + "\t\t<met:md1041 " + this.isNumber(this.nilaiAwalPembiayaan) + "contextRef=\"c\">" + this.nilaiAwalPembiayaan + "</met:md1041>\n";
        }
        if (this.kualitas != null) {
            xbrl = xbrl + "\t\t<SF:ld1 " + this.isNumber(this.kualitas) + "contextRef=\"c\">" + this.kualitas + "</SF:ld1>\n";
        }
        if (this.jenisValuta != null) {
            xbrl = xbrl + "\t\t<MU:ld1 " + this.isNumber(this.jenisValuta) + "contextRef=\"c\">" + this.jenisValuta + "</MU:ld1>\n";
        }
        if (this.tagihanPiutangPembiayaanBrutoDalamMataUangAsal != null) {
            xbrl = xbrl + "\t\t<met:md1042 " + this.isNumber(this.tagihanPiutangPembiayaanBrutoDalamMataUangAsal) + "contextRef=\"c\">" + this.tagihanPiutangPembiayaanBrutoDalamMataUangAsal + "</met:md1042>\n";
        }
        if (this.tagihanPiutangPembiayaanBrutoDalamEquivalenRupiah != null) {
            xbrl = xbrl + "\t\t<met:md1043 " + this.isNumber(this.tagihanPiutangPembiayaanBrutoDalamEquivalenRupiah) + "contextRef=\"c\">" + this.tagihanPiutangPembiayaanBrutoDalamEquivalenRupiah + "</met:md1043>\n";
        }
        if (this.bungaBagiHasilYangDiTangguhkanDalamMataUangAsal != null) {
            xbrl = xbrl + "\t\t<met:md1044 " + this.isNumber(this.bungaBagiHasilYangDiTangguhkanDalamMataUangAsal) + "contextRef=\"c\">" + this.bungaBagiHasilYangDiTangguhkanDalamMataUangAsal + "</met:md1044>\n";
        }
        if (this.bungaBagiHasilYangDiTangguhkanDalamEquivalenRupiah != null) {
            xbrl = xbrl + "\t\t<met:md1045 " + this.isNumber(this.bungaBagiHasilYangDiTangguhkanDalamEquivalenRupiah) + "contextRef=\"c\">" + this.bungaBagiHasilYangDiTangguhkanDalamEquivalenRupiah + "</met:md1045>\n";
        }
        if (this.piutangPembiayaanPokokDalamMataUangAsal != null) {
            xbrl = xbrl + "\t\t<met:md586 " + this.isNumber(this.piutangPembiayaanPokokDalamMataUangAsal) + "contextRef=\"c\">" + this.piutangPembiayaanPokokDalamMataUangAsal + "</met:md586>\n";
        }
        if (this.piutangPembiayaanPokokDalamEquivalenRupiah != null) {
            xbrl = xbrl + "\t\t<met:md124 " + this.isNumber(this.piutangPembiayaanPokokDalamEquivalenRupiah) + "contextRef=\"c\">" + this.piutangPembiayaanPokokDalamEquivalenRupiah + "</met:md124>\n";
        }
        if (this.metodePembentukanCadanganKerugian != null) {
            xbrl = xbrl + "\t\t<JK:ld20 " + this.isNumber(this.metodePembentukanCadanganKerugian) + "contextRef=\"c\">" + this.metodePembentukanCadanganKerugian + "</JK:ld20>\n";
        }
        if (this.nilaiAsetBaikCadanganKerugianPenurunanNilai != null) {
            xbrl = xbrl + "\t\t<met:md1291 " + this.isNumber(this.nilaiAsetBaikCadanganKerugianPenurunanNilai) + "contextRef=\"c\">" + this.nilaiAsetBaikCadanganKerugianPenurunanNilai + "</met:md1291>\n";
        }
        if (this.nilaiAsetKurangBaikCadanganKerugianPenurunanNilai != null) {
            xbrl = xbrl + "\t\t<met:md1292 " + this.isNumber(this.nilaiAsetKurangBaikCadanganKerugianPenurunanNilai) + "contextRef=\"c\">" + this.nilaiAsetKurangBaikCadanganKerugianPenurunanNilai + "</met:md1292>\n";
        }
        if (this.nilaiAsetTidakBaikCadanganKerugianPenurunanNilai != null) {
            xbrl = xbrl + "\t\t<met:md1293 " + this.isNumber(this.nilaiAsetTidakBaikCadanganKerugianPenurunanNilai) + "contextRef=\"c\">" + this.nilaiAsetTidakBaikCadanganKerugianPenurunanNilai + "</met:md1293>\n";
        }
        if (this.proporsiPenjaminanPembiayaan != null) {
            xbrl = xbrl + "\t\t<met:pd896 " + this.isNumber(this.proporsiPenjaminanPembiayaan) + "contextRef=\"c\">" + this.proporsiPenjaminanPembiayaan + "</met:pd896>\n";
        }
        if (this.namaPasanganUsahaDebitur != null) {
            xbrl = xbrl + "\t\t<met:sd20 " + this.isNumber(this.namaPasanganUsahaDebitur) + "contextRef=\"c\">" + this.namaPasanganUsahaDebitur + "</met:sd20>\n";
        }
        if (this.bentukPasanganUsahaDebitur != null) {
            xbrl = xbrl + "\t\t<EN:ld11 " + this.isNumber(this.bentukPasanganUsahaDebitur) + "contextRef=\"c\">" + this.bentukPasanganUsahaDebitur + "</EN:ld11>\n";
        }
        if (this.kategoriUsahaPasanganDebitur != null) {
            xbrl = xbrl + "\t\t<EN:ld5 " + this.isNumber(this.kategoriUsahaPasanganDebitur) + "contextRef=\"c\">" + this.kategoriUsahaPasanganDebitur + "</EN:ld5>\n";
        }
        if (this.kategoriUsahaKeuanganBerkelanjutan != null) {
            xbrl = xbrl + "\t\t<EN:ld20 " + this.isNumber(this.kategoriUsahaKeuanganBerkelanjutan) + "contextRef=\"c\">" + this.kategoriUsahaKeuanganBerkelanjutan + "</EN:ld20>\n";
        }
        if (this.golonganPasanganUsahaDebitur != null) {
            xbrl = xbrl + "\t\t<EN:ld9 " + this.isNumber(this.golonganPasanganUsahaDebitur) + "contextRef=\"c\">" + this.golonganPasanganUsahaDebitur + "</EN:ld9>\n";
        }
        if (this.statusKeterkaitan != null) {
            xbrl = xbrl + "\t\t<KT:ld1 " + this.isNumber(this.statusKeterkaitan) + "contextRef=\"c\">" + this.statusKeterkaitan + "</KT:ld1>\n";
        }
        if (this.lokasiKabupatenKota != null) {
            xbrl = xbrl + "\t\t<LO:ld7 " + this.isNumber(this.lokasiKabupatenKota) + "contextRef=\"c\">" + this.lokasiKabupatenKota + "</LO:ld7>\n";
        }
        if (this.sektorEknomi != null) {
            xbrl = xbrl + "\t\t<SE:ld1 " + this.isNumber(this.sektorEknomi) + "contextRef=\"c\">" + this.sektorEknomi + "</SE:ld1>\n";
        }
        if (this.f214021300_col != null) {
            xbrl += this.f214021300_col.buildStrXBRL();
        }
        return xbrl + "</F214021300:Fac>\n";
    }
    
    private String isNumber(final Object obj) {
        try {
            Double.parseDouble(obj.toString());
            return "decimals=\"0\" unitRef=\"I\" ";
        }
        catch (NumberFormatException ex) {
            return "";
        }
    }
    
    public static class F214021300_COL
    {
        private String id;
        private String nomorAgunan;
        private String jenisAgunan;
        private String nilaiAgunan;
        
        public String getId() {
            return this.id;
        }
        
        public void setId(final String id) {
            this.id = id;
        }
        
        public String getNomorAgunan() {
            return this.nomorAgunan;
        }
        
        public void setNomorAgunan(final String nomorAgunan) {
            this.nomorAgunan = nomorAgunan;
        }
        
        public String getJenisAgunan() {
            return this.jenisAgunan;
        }
        
        public void setJenisAgunan(final String jenisAgunan) {
            this.jenisAgunan = jenisAgunan;
        }
        
        public String getNilaiAgunan() {
            return this.nilaiAgunan;
        }
        
        public void setNilaiAgunan(final String nilaiAgunan) {
            this.nilaiAgunan = nilaiAgunan;
        }
        
        public String buildStrXBRL() {
            String xbrl = "\t\t<F214021300:Col id=\"" + this.id + "\">\n";
            if (this.nomorAgunan != null) {
                xbrl = xbrl + "\t\t\t<met:sd662 contextRef=\"c\">" + this.nomorAgunan + "</met:sd662>\n";
            }
            if (this.jenisAgunan != null) {
                xbrl = xbrl + "\t\t\t<BJ:ld2 " + this.isNumber(this.jenisAgunan) + "contextRef=\"c\">" + this.jenisAgunan + "</BJ:ld2>\n";
            }
            if (this.nilaiAgunan != null) {
                xbrl = xbrl + "\t\t\t<met:md270 " + this.isNumber(this.nilaiAgunan) + "contextRef=\"c\">" + this.nilaiAgunan + "</met:md270>\n";
            }
            return xbrl + "\t\t</F214021300:Col>\n";
        }
        
        private String isNumber(final Object obj) {
            try {
                Double.parseDouble(obj.toString());
                return "decimals=\"0\" unitRef=\"I\" ";
            }
            catch (NumberFormatException ex) {
                return "";
            }
        }
    }
}
